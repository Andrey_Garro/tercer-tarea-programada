from tkinter import *
from os import listdir

class Cargar_guardar_foto:
    """
    carga en una lista de strings los nombres de la fotos
    """
    def __init__(self):
        self.lista_fotos = []

    def obtener_nombre(self):
        for x in listdir("imgs/fotos/"):
            if x.endswith("png"):
                self.lista_fotos.append(x)
        return self.lista_fotos


