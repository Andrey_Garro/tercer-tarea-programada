import tornado.web
from tornado.ioloop import IOLoop
import base64



class PokemonHandler(tornado.web.RequestHandler):
    def post(self):

        try:
            pokemon_name = self.get_argument("pokemon")
            img_base64 = self.get_argument("photo")

            with open("imgs/fotos/{}.png".format(pokemon_name), "wb") as file_writer:
                file_writer.write(base64.b64decode(img_base64))

            self.write("true")
        except Exception as ex:
            print(ex)
            self.write("false")

class ControlHandler(tornado.web.RequestHandler):
    def post(self):
        global accion
        try:
            accion = self.get_argument("movimiento")
            if accion == "arriba":
                accion = "arriba"
            if accion == "abajo":
                accion = "abajo"
            if accion == "derecha":
                accion = "derecha"
            if accion == "izquierda":
                accion = "izquierda"
            if accion == "start":
                accion = "start"
            if accion == "espacio":
                accion = "espacio"
        except Exception as ex:
            print(ex)
            self.write("false")


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/api/v1/pokemon",PokemonHandler),
            (r"/api/v1/control", ControlHandler)
        ]

        tornado.web.Application.__init__(self, handlers)

def cambiar_accion():
    """
    cambia el valor de accion para que no se ejecute el mismo movimiento repetidas veces
    :return:
    """
    global accion
    accion = ""

def main():
    """
    funcion principal que ejecuta los handler
    """
    global accion
    accion = ""
    app = Application()
    app.listen(8080)
    IOLoop.instance().start()

def accion_rec():
    """
    retorma el valor de accion
    :return:
    """
    return accion

if __name__ == "__main__":
    main()