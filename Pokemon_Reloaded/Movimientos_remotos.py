from pokemon_reloaded import *

class Movimientos_remotos():
    def __init__(self, canvas, orden_move):
        self.canvas = canvas
        self.orden_move = orden_move

    def arriba_lab(self):
        return movimiento_lab_ash.mover_arriba, movimiento_lab_pika.mover_arriba

    def abajo_lab(self):
        return movimiento_lab_ash.mover_abajo, movimiento_lab_pika.mover_abajo

    def derecha_lab(self):
        return movimiento_lab_ash.mover_derecha, movimiento_lab_pika.mover_derecha

    def izquierda_lab(self):
        return movimiento_lab_ash.mover_izquierda, movimiento_lab_pika.mover_izquierda

    def arriba_paleta(self):
        return movimiento_paleta_ash.mover_arriba, movimiento_paleta_pika.mover_arriba

    def abajo_paleta(self):
        return movimiento_paleta_ash.mover_abajo, movimiento_paleta_pika.mover_abajo

    def derecha_paleta(self):
        return movimiento_paleta_ash.mover_derecha, movimiento_paleta_pika.mover_derecha

    def izquierda_paleta(self):
        return movimiento_paleta_ash.mover_izquierda, movimiento_paleta_pika.mover_izquierda

    def arriba_bosque(self):
        return movimiento_bosque_ash.mover_arriba, movimiento_bosque_pika.mover_arriba

    def abajo_bosque(self):
        return movimiento_bosque_ash.mover_abajo, movimiento_bosque_pika.mover_abajo

    def derecha_bosque(self):
        return movimiento_bosque_ash.mover_derecha, movimiento_bosque_pika.mover_derecha

    def izquierda_bosque(self):
        return movimiento_bosque_ash.mover_izquierda, movimiento_bosque_pika.mover_izquierda