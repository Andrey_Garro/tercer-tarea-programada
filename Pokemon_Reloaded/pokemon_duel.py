from tkinter import *
import os
import time
import pygame
from tkinter import messagebox
from tkinter import messagebox
import random
from threading import Thread


def load_img(name):
    """
    carga las imagenes desde la carpeta de imgs
    :param name: recibe nombre de la variable
    :return: la imagen
    """
    path = os.path.join("imgs", name)
    img = PhotoImage(file=path)
    return img


ventana_batalla = Tk()
ventana_batalla.title("Pokemon Reloaded")
ventana_batalla.minsize(550, 700)
ventana_batalla.resizable(width=False, height=False)
ventana_batalla.config(bg='white')
canvas_fondo_batalla = Canvas(ventana_batalla, width=550, height=700)
fondo_batalla = load_img("fondo_batalla1.png")
canvas_fondo_batalla.create_image(0, 0, anchor=NW, image=fondo_batalla)
fondo_batalla_random = load_img("fondo_ruleta.png")
ficha_venasaur = load_img("venasaur_ficha.png")
ficha_blastoise = load_img("blastoise_ficha.png")
ficha_pidgeot = load_img("pidgeot_ficha.png")
ficha_raticate = load_img("raticate_ficha.png")
ficha_raichu = load_img("raichu_ficha.png")
ficha_charizard = load_img("charizard_ficha.png")
cursor_azul = load_img("cursor_azul.png")
cursor_rojo = load_img("cursor_rojo.png")
canvas_fondo_batalla.create_image(160, 65, image=ficha_blastoise, anchor=CENTER, tag="Blastoise")
canvas_fondo_batalla.create_image(345, 65, image=ficha_charizard, anchor=CENTER, tag="Charizard")
canvas_fondo_batalla.create_image(265, 65, image=ficha_pidgeot, anchor=CENTER, tag="Pidgeot")
canvas_fondo_batalla.create_image(200, 500, image=ficha_raichu, anchor=CENTER, tag="Raichu")
canvas_fondo_batalla.create_image(99, 500, image=ficha_raticate, anchor=CENTER, tag="Raticate")
canvas_fondo_batalla.create_image(296, 500, image=ficha_venasaur, anchor=CENTER, tag="Venasaur")
canvas_fondo_batalla.create_image(99999999, 99999999, image=cursor_rojo, anchor=CENTER, tag="cursor")
canvas_fondo_batalla.create_image(99999999, 99999999, image=cursor_azul, anchor=CENTER, tag="cursor2")
canvas_fondo_batalla.pack()
pokemon_1 = "Raichu"
pokemon_1_2 = "Raticate"
pokemon_1_3 = "Venasaur"
pokemon_2 = "Blastoise"
pokemon_2_2 = "Charizard"
pokemon_2_3 = "Pidgeot"

mis_pokemones = [pokemon_1, pokemon_1_2, pokemon_1_3]
sus_pokemones = [pokemon_2, pokemon_2_2, pokemon_2_3]

pygame.init()
pygame.mixer.init()
pygame.mixer.music.load("batalla_tablero.ogg")
pygame.mixer.music.play(30)

def coordes(objeto):
    return canvas_fondo_batalla.coords(objeto)


def pelea_pokemon(pokemon):
    for x in range(len(sus_pokemones)):
        if coordes(sus_pokemones[x])[0] == coordes(pokemon)[0] + 62 and coordes(sus_pokemones[x])[1] == \
                coordes(pokemon)[1] or coordes(sus_pokemones[x])[0] == coordes(pokemon)[0] - 62 and \
                        coordes(sus_pokemones[x])[1] == coordes(pokemon)[1] or coordes(sus_pokemones[x])[0] ==\
                coordes(pokemon)[
                    0] and coordes(sus_pokemones[x])[1] == coordes(pokemon)[1] + 74 or coordes(sus_pokemones[x])[0] ==\
            coordes(pokemon)[
                0] and coordes(sus_pokemones[x])[
                           1] == coordes(pokemon)[1] - 74:
            canvas_batalla_random = Canvas(canvas_fondo_batalla, width=550, height=700)
            canvas_batalla_random.create_image(0,0, image=fondo_batalla_random, anchor=NW)
            canvas_batalla_random.config(bg="white")
            canvas_batalla_random.pack()
            aleatorio1 = random.choice(range(1, 100))
            aleatorio2 = random.choice(range(1, 100))
            cont = 0
            cont1 = random.randrange(1, 100)
            cont2 = random.randrange(1, 100)

            def random1(cont):
                while cont <= cont1:
                    kk1 = Label(canvas_batalla_random, height=2, width=10, bg="white", text=cont,
                                font=("Papyrus", 10)).place(x=274, y=270)
                    canvas_batalla_random.update()
                    cont += 1
                    time.sleep(0.04)



            def random2(cont):
                while cont <= cont2:
                    kk = Label(canvas_batalla_random, height=2, width=10, bg="white", text=cont, font=("Papyrus", 10)).place(
                        x=186, y=370)
                    canvas_batalla_random.update()
                    cont += 1
                    time.sleep(0.04)

            kk = Label(canvas_batalla_random, height=2, width=10, bg="white", text=cont, font=("Papyrus", 10)).place(x=186,
                                                                                                                y=370)
            kk1 = Label(canvas_batalla_random, height=2, width=10, bg="white", text=cont, font=("Papyrus", 10)).place(x=274,
                                                                                                                 y=270)
            random1(cont)
            random2(cont)
            if cont2 > cont1:
                canvas_batalla_random.forget()
                canvas_fondo_batalla.pack()
                if sus_pokemones[x] == "Blastoise":
                    canvas_fondo_batalla.coords(sus_pokemones[x], 160, 65)
                elif sus_pokemones[x] == "Charizard":
                    canvas_fondo_batalla.coords(sus_pokemones[x], 345, 65)
                elif sus_pokemones[x] == "Pidgeot":
                    canvas_fondo_batalla.coords(sus_pokemones[x], 265, 65)
            else:
                canvas_batalla_random.forget()
                canvas_fondo_batalla.pack()
                if pokemon == "Raichu":
                    canvas_fondo_batalla.coords(pokemon, 200, 500)
                elif pokemon == "Raticate":
                    canvas_fondo_batalla.coords(pokemon, 99, 500)
                elif pokemon == "Venasaur":
                    canvas_fondo_batalla.coords(pokemon, 296, 500)


        elif coordes(mis_pokemones[x])[0] == coordes(pokemon)[0] + 62 and coordes(mis_pokemones[x])[1] == \
                coordes(pokemon)[1] or coordes(mis_pokemones[x])[0] == coordes(pokemon)[0] - 62 and \
                        coordes(mis_pokemones[x])[1] == coordes(pokemon)[1] or coordes(mis_pokemones[x])[0] ==\
                coordes(pokemon)[
                    0] and coordes(mis_pokemones[x])[1] == coordes(pokemon)[1] + 74 or coordes(mis_pokemones[x])[0] ==\
            coordes(pokemon)[
                0] and coordes(mis_pokemones[x])[
                           1] == coordes(pokemon)[1] - 74:
            canvas_batalla_random = Canvas(canvas_fondo_batalla, width=550, height=700)
            canvas_batalla_random.create_image(0,0, image=fondo_batalla_random, anchor=NW)
            canvas_batalla_random.config(bg="white")
            canvas_batalla_random.pack()
            aleatorio1 = random.choice(range(1, 100))
            aleatorio2 = random.choice(range(1, 100))
            cont = 0
            cont1 = random.randrange(1, 100)
            cont2 = random.randrange(1, 100)

            def random1(cont):
                while cont <= cont1:
                    kk1 = Label(canvas_batalla_random, height=2, width=10, bg="white", text=cont,
                                font=("Papyrus", 10)).place(x=274, y=270)
                    canvas_batalla_random.update()
                    cont += 1
                    time.sleep(0.04)



            def random2(cont):
                while cont <= cont2:
                    kk = Label(canvas_batalla_random, height=2, width=10, bg="white", text=cont, font=("Papyrus", 10)).place(
                        x=186, y=370)
                    canvas_batalla_random.update()
                    cont += 1
                    time.sleep(0.04)

            lbl_1 = Label(canvas_batalla_random, height=2, width=10, bg="white", text=cont, font=("Papyrus", 10)).place(x=186,
                                                                                                                y=370)
            lbl_2 = Label(canvas_batalla_random, height=2, width=10, bg="white", text=cont, font=("Papyrus", 10)).place(x=274,
                                                                                                                 y=270)
            random1(cont)
            random2(cont)
            if cont2 < cont1:
                canvas_batalla_random.forget()
                canvas_fondo_batalla.pack()
                if mis_pokemones[x] == "Raichu":
                    canvas_fondo_batalla.coords(mis_pokemones[x], 200, 500)
                elif mis_pokemones[x] == "Raticate":
                    canvas_fondo_batalla.coords(mis_pokemones[x], 99, 500)
                elif mis_pokemones[x] == "Venasaur":
                    canvas_fondo_batalla.coords(mis_pokemones[x], 296, 500)
            else:
                canvas_batalla_random.forget()
                canvas_fondo_batalla.pack()
                if pokemon == "Blastoise":
                    canvas_fondo_batalla.coords(pokemon, 160, 65)
                elif pokemon == "Charizard":
                    canvas_fondo_batalla.coords(pokemon, 345, 65)
                elif pokemon == "Pidgeot":
                    canvas_fondo_batalla.coords(pokemon, 265, 65)



def movimientos_flecha(tag):
    if tag == "cursor":
        def mover_abajo(event):
            if coordes(tag)[1] == 383 and coordes(mis_pokemones[0]) != [200, 500] and \
                            coordes(mis_pokemones[1]) != [99, 500] and coordes(mis_pokemones[2]) != [296, 500]:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif coordes(tag) == [272.0, 383.0] and coordes(mis_pokemones[0]) == [280.0, 420.0]:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif (coordes(mis_pokemones[0]) == [280.0, 420.0] or coordes(mis_pokemones[1]) == [280.0, 420.0] or coordes(
                    mis_pokemones[2]) == [280.0, 420.0]) and coordes(tag)[1] == 383:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif "<KeyPress-Down>":
                canvas_fondo_batalla.move(tag, 0, 74)
                if coordes(tag)[1] >= 440.0:
                    canvas_fondo_batalla.coords(tag, 200, 450)

        def mover_arriba(event):
            if coordes(tag)[1] >= 440:
                canvas_fondo_batalla.coords(tag, 272.0, 383.0)
            elif coordes(tag)[1] <= 87:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif "<KeyPress-Up>":
                canvas_fondo_batalla.move(tag, 0, -74)

        def mover_derecha(event):
            if (coordes(tag)[0] + 62) > 500:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif coordes(tag) == [200, 450]:
                canvas_fondo_batalla.coords(tag, 296, 450)
            elif coordes(tag) == [296, 450]:
                canvas_fondo_batalla.coords(tag, 99, 450)
            elif coordes(tag) == [99, 450]:
                canvas_fondo_batalla.coords(tag, 200, 450)
            elif "<KeyPress-Right>":
                canvas_fondo_batalla.move(tag, 62, 0)

        def mover_izquierda(event):
            if (coordes(tag)[0] - 62) < 50:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif coordes(tag) == [200, 450]:
                canvas_fondo_batalla.coords(tag, 99, 450)
            elif coordes(tag) == [296, 450]:
                canvas_fondo_batalla.coords(tag, 200, 450)
            elif coordes(tag) == [99, 450]:
                canvas_fondo_batalla.coords(tag, 296, 450)
            elif "<KeyPress-Left>":
                canvas_fondo_batalla.move(tag, -62, 0)

        ventana_batalla.bind("<Down>", mover_abajo)
        ventana_batalla.bind("<Up>", mover_arriba)
        ventana_batalla.bind("<Left>", mover_izquierda)
        ventana_batalla.bind("<Right>", mover_derecha)
    elif tag == "cursor2":
        def mover_abajo2(event):
            if coordes(tag)[1] == 383:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif coordes(tag)[1] == 10:
                canvas_fondo_batalla.coords(tag, 272.0, 87.0)
            elif "<KeyPress-Down>":
                canvas_fondo_batalla.move(tag, 0, 74)
                if coordes(tag)[1] >= 440.0:
                    canvas_fondo_batalla.coords(tag, 200, 450)

        def mover_arriba2(event):
            if coordes(tag)[1] == 87 and coordes(sus_pokemones[0]) != [160, 65] and \
                            coordes(sus_pokemones[1]) != [345, 65] and coordes(sus_pokemones[2]) != [265, 65]:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif (coordes(sus_pokemones[0]) == [280, 124] or coordes(sus_pokemones[1]) == [280, 124] or coordes(sus_pokemones[2]) == [
                280, 124]) and coordes(tag)[1] == 87.0:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif coordes(tag)[1] == 87:
                canvas_fondo_batalla.coords(tag, 160, 10)
            elif coordes(tag)[1] >= 440:
                canvas_fondo_batalla.coords(tag, 272.0, 383.0)
            elif coordes(tag)[1] <= 87:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif "<KeyPress-Up>":
                canvas_fondo_batalla.move(tag, 0, -74)

        def mover_derecha2(event):
            if (coordes(tag)[0] + 62) > 500:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif coordes(tag) == [160, 10]:
                canvas_fondo_batalla.coords(tag, 265, 10)
            elif coordes(tag) == [265, 10]:
                canvas_fondo_batalla.coords(tag, 345, 10)
            elif coordes(tag) == [345, 10]:
                canvas_fondo_batalla.coords(tag, 160, 10)
            elif "<KeyPress-Right>":
                canvas_fondo_batalla.move(tag, 62, 0)


        def mover_izquierda2(event):
            if (coordes(tag)[0] - 62) < 50:
                canvas_fondo_batalla.move(tag, 0, 0)
            elif coordes(tag) == [160, 10]:
                canvas_fondo_batalla.coords(tag, 345, 10)
            elif coordes(tag) == [345, 10]:
                canvas_fondo_batalla.coords(tag, 265, 10)
            elif coordes(tag) == [265, 10]:
                canvas_fondo_batalla.coords(tag, 160, 10)
            elif "<KeyPress-Left>":
                canvas_fondo_batalla.move(tag, -62, 0)

        ventana_batalla.bind("<Down>", mover_abajo2)
        ventana_batalla.bind("<Up>", mover_arriba2)
        ventana_batalla.bind("<Left>", mover_izquierda2)
        ventana_batalla.bind("<Right>", mover_derecha2)

    def enter(event):
        if tag == "cursor":
            for pokemon in range(len(mis_pokemones)):
                if coordes(tag) == [200, 450] and coordes(mis_pokemones[pokemon]) == [200, 500]:
                    canvas_fondo_batalla.coords(tag, 99999999, 999999)
                    canvas_fondo_batalla.coords(mis_pokemones[pokemon], 280.0, 420.0)
                    matriz_coords_batalla(mis_pokemones[pokemon], tag)
                    pelea_pokemon(mis_pokemones[pokemon])
                    turnos_jugadores(2)
                elif coordes(tag) == [99, 450] and coordes(mis_pokemones[pokemon]) == [99, 500]:
                    canvas_fondo_batalla.coords(tag, 99999999, 999999)
                    canvas_fondo_batalla.coords(mis_pokemones[pokemon], 280.0, 420.0)
                    matriz_coords_batalla(mis_pokemones[pokemon], tag)
                    pelea_pokemon(mis_pokemones[pokemon])
                    turnos_jugadores(2)
                elif coordes(tag) == [296, 450] and coordes(mis_pokemones[pokemon]) == [296, 500]:
                    canvas_fondo_batalla.coords(tag, 99999999, 999999)
                    canvas_fondo_batalla.coords(mis_pokemones[pokemon], 280.0, 420.0)
                    matriz_coords_batalla(mis_pokemones[pokemon], tag)
                    pelea_pokemon(mis_pokemones[pokemon])
                    turnos_jugadores(2)
                elif (coordes(tag)[1] + 37) == coordes(mis_pokemones[pokemon])[1] and \
                                (coordes(tag)[0] + 8) == coordes(mis_pokemones[pokemon])[0]:
                    canvas_fondo_batalla.coords(tag, 99999999, 999999)
                    movimientos_pokemon(mis_pokemones[pokemon], tag)
        elif tag == "cursor2":
            for pokemon in range(len(mis_pokemones)):
                if coordes(tag) == [160, 10] and coordes(sus_pokemones[pokemon]) == [160, 65]:
                    canvas_fondo_batalla.coords(tag, 99999999, 999999)
                    canvas_fondo_batalla.coords(sus_pokemones[pokemon], 280, 124)
                    matriz_coords_batalla(sus_pokemones[pokemon], tag)
                    pelea_pokemon(sus_pokemones[pokemon])
                    turnos_jugadores(1)
                elif coordes(tag) == [265, 10] and coordes(sus_pokemones[pokemon]) == [265, 65]:
                    canvas_fondo_batalla.coords(tag, 99999999, 999999)
                    canvas_fondo_batalla.coords(sus_pokemones[pokemon], 280, 124)
                    matriz_coords_batalla(sus_pokemones[pokemon], tag)
                    pelea_pokemon(sus_pokemones[pokemon])
                    turnos_jugadores(1)
                elif coordes(tag) == [345, 10] and coordes(sus_pokemones[pokemon]) == [345, 65]:
                    canvas_fondo_batalla.coords(tag, 99999999, 999999)
                    canvas_fondo_batalla.coords(sus_pokemones[pokemon], 280, 124)
                    matriz_coords_batalla(sus_pokemones[pokemon], tag)
                    pelea_pokemon(sus_pokemones[pokemon])
                    turnos_jugadores(1)
                elif (coordes(tag)[1] + 37) == coordes(sus_pokemones[pokemon])[1] and \
                                (coordes(tag)[0] + 8) == coordes(sus_pokemones[pokemon])[0]:
                    canvas_fondo_batalla.coords(tag, 99999999, 999999)
                    movimientos_pokemon(sus_pokemones[pokemon], tag)


    ventana_batalla.bind("<Return>", ubicar_pokemon)
    ventana_batalla.bind("<BackSpace>", enter)

def colisiones_pokemones_pokemon(pokemon, siguiente_posicion):
    lista_pokemon = [mis_pokemones[0], mis_pokemones[1], mis_pokemones[2], pokemon_2, pokemon_2_2, pokemon_2_3]
    for poke in lista_pokemon:
        if siguiente_posicion == "derecha":
            if (coordes(pokemon)[0] + 62) == coordes(poke)[0] and \
                            coordes(pokemon)[1] == coordes(poke)[1]:
                return True
            elif (coordes(pokemon)[0] + 62) >= 500:
                return True
        elif siguiente_posicion == "izquierda":
            if (coordes(pokemon)[0] - 62) == coordes(poke)[0] and \
                            coordes(pokemon)[1] == coordes(poke)[1]:
                return True
            elif (coordes(pokemon)[0] - 62) <= 50:
                return True
        elif siguiente_posicion == "abajo":
            if (coordes(pokemon)[1] + 74) == coordes(poke)[1] and \
                            coordes(pokemon)[0] == coordes(poke)[0]:
                return True
            elif (coordes(pokemon)[1] +74) >= 494.0:
                return True
        elif siguiente_posicion == "arriba":
            if (coordes(pokemon)[1] - 74) == coordes(poke)[1] and \
                            coordes(pokemon)[0] == coordes(poke)[0]:
                return True
            elif  (coordes(pokemon)[1] - 74) <= 50:
                return True


def movimientos_pokemon(pokemon, tag):
    if tag == "cursor":
        turno = 2
    elif tag == "cursor2":
        turno = 1

    def transicion_imagen_abajo(imagen, movimiento):
        contador = 0
        ventana_batalla.unbind("<Down>")
        ventana_batalla.unbind("<Up>")
        ventana_batalla.unbind("<Left>")
        ventana_batalla.unbind("<Right>")
        while contador < 4:
            canvas_fondo_batalla.move(imagen, 0, 74/4)
            time.sleep(0.05)
            canvas_fondo_batalla.update()
            contador += 1

    def transicion_imagen_arriba(imagen, movimiento):
        contador = 0
        ventana_batalla.unbind("<Down>")
        ventana_batalla.unbind("<Up>")
        ventana_batalla.unbind("<Left>")
        ventana_batalla.unbind("<Right>")
        while contador < 4:
            canvas_fondo_batalla.move(imagen, 0, -74/4)
            time.sleep(0.05)
            canvas_fondo_batalla.update()
            contador += 1

    def transicion_imagen_izquierda(imagen, movimiento):
        contador = 0
        ventana_batalla.unbind("<Down>")
        ventana_batalla.unbind("<Up>")
        ventana_batalla.unbind("<Left>")
        ventana_batalla.unbind("<Right>")
        while contador < 4:
            canvas_fondo_batalla.move(imagen, -62/4, 0)
            time.sleep(0.05)
            canvas_fondo_batalla.update()
            contador += 1

    def transicion_imagen_derecha(imagen, movimiento):
        contador = 0
        ventana_batalla.unbind("<Down>")
        ventana_batalla.unbind("<Up>")
        ventana_batalla.unbind("<Left>")
        ventana_batalla.unbind("<Right>")
        while contador < 4:
            canvas_fondo_batalla.move(imagen, 62/4, 0)
            time.sleep(0.05)
            canvas_fondo_batalla.update()
            contador += 1

    def abajo_pokemon(event):
        if colisiones_pokemones_pokemon(pokemon, "abajo") == True:
            canvas_fondo_batalla.move(pokemon, 0, 0)
        else:
            borrar_matriz_coords_batalla(pokemon)
            #canvas_fondo_batalla.move(pokemon, 0, 74)
            transicion_imagen_abajo(pokemon, 74)
            pelea_pokemon(pokemon)
            matriz_coords_batalla(pokemon, tag)
            turnos_jugadores(turno)

    def arriba_pokemon(event):
        if colisiones_pokemones_pokemon(pokemon, "arriba") == True:
            canvas_fondo_batalla.move(pokemon, 0, 0)
        else:
            borrar_matriz_coords_batalla(pokemon)
            #canvas_fondo_batalla.move(pokemon, 0, -74)
            transicion_imagen_arriba(pokemon, -74)
            pelea_pokemon(pokemon)
            matriz_coords_batalla(pokemon, tag)
            turnos_jugadores(turno)

    def izquierda_pokemon(event):
        if colisiones_pokemones_pokemon(pokemon, "izquierda") == True:
            canvas_fondo_batalla.move(pokemon, 0, 0)
        else:
            borrar_matriz_coords_batalla(pokemon)
            #canvas_fondo_batalla.move(pokemon, -62, 0)
            transicion_imagen_izquierda(pokemon, -62)
            pelea_pokemon(pokemon)
            matriz_coords_batalla(pokemon, tag)
            turnos_jugadores(turno)

    def derecha_pokemon(event):
        if colisiones_pokemones_pokemon(pokemon, "derecha") == True:
            canvas_fondo_batalla.move(pokemon, 0, 0)
        else:
            borrar_matriz_coords_batalla(pokemon)
            #canvas_fondo_batalla.move(pokemon, 62, 0)
            transicion_imagen_derecha(pokemon, 62)
            pelea_pokemon(pokemon)
            matriz_coords_batalla(pokemon, tag)
            turnos_jugadores(turno)

    ventana_batalla.bind("<Down>", abajo_pokemon)
    ventana_batalla.bind("<Up>", arriba_pokemon)
    ventana_batalla.bind("<Left>", izquierda_pokemon)
    ventana_batalla.bind("<Right>", derecha_pokemon)
    ventana_batalla.bind("<BackSpace>", lambda event: movimientos_flecha("cursor"))


def turnos_jugadores(turno):
    turno_jugador = turno
    if turno_jugador == 1:
        canvas_fondo_batalla.coords("cursor", 272.0, 383.0)
        return movimientos_flecha("cursor")

    if turno_jugador == 2:
        canvas_fondo_batalla.coords("cursor2", 272.0, 87.0)
        return movimientos_flecha("cursor2")


def matriz_coords_batalla(pokemon, tag):
    for fila in range(len(matriz_coords)):
        for columna in range(len(matriz_coords[0])):
            if canvas_fondo_batalla.coords(pokemon) == matriz_coords[fila][columna]:
                if tag == "cursor":
                    matriz_batalla[fila][columna] = 1
                    if len(juego["jugador1"]["pokemones_tablero"]) == 0:
                        juego["jugador1"]["pokemones_tablero"].append(
                            {"nombre": pokemon, "posicion": (fila, columna)})
                    for y in range(len(juego["jugador1"]["pokemones_banca"])):
                        if juego["jugador1"]["pokemones_banca"][y]["nombre"] == pokemon:
                            juego["jugador1"]["pokemones_banca"][y]["nombre"] = ""
                    for x in range(len(juego["jugador1"]["pokemones_tablero"])):
                        if juego["jugador1"]["pokemones_tablero"][x]["nombre"] == pokemon:
                            juego["jugador1"]["pokemones_tablero"][x]["posicion"] = (fila, columna)
                        else:
                            juego["jugador1"]["pokemones_tablero"].append(
                                {"nombre": pokemon, "posicion": (fila, columna)})

                elif tag == "cursor2":
                    matriz_batalla[fila][columna] = 2
                    if len(juego["jugador2"]["pokemones_tablero"]) == 0:
                        juego["jugador2"]["pokemones_tablero"].append(
                            {"nombre": pokemon, "posicion": (fila, columna)})
                    for y in range(len(juego["jugador2"]["pokemones_banca"])):
                        if juego["jugador2"]["pokemones_banca"][y]["nombre"] == pokemon:
                            juego["jugador2"]["pokemones_banca"][y]["nombre"] = ""
                    for x in range(len(juego["jugador2"]["pokemones_tablero"])):
                        if juego["jugador2"]["pokemones_tablero"][x]["nombre"] == pokemon:
                            juego["jugador2"]["pokemones_tablero"][x]["posicion"] = (fila, columna)




def borrar_matriz_coords_batalla(pokemon):
    for fila in range(len(matriz_coords)):
        for columna in range(len(matriz_coords[0])):
            if canvas_fondo_batalla.coords(pokemon) == matriz_coords[fila][columna]:
                matriz_batalla[fila][columna] = 0


matriz_batalla = [[0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0]]

matriz_coords = [[[94, 124], [156, 124], [218, 124], [280, 124], [342, 124], [404, 124], [466, 124]],
                 [[94, 198], [156, 198], [218, 198], [280, 198], [342, 198], [404, 198], [466, 198]],
                 [[94, 272], [156, 272], [218, 272], [280, 272], [342, 272], [404, 272], [466, 272]],
                 [[94, 346], [156, 346], [218, 346], [280, 346], [342, 346], [404, 346], [466, 346]],
                 [[94, 420], [156, 420], [218, 420], [280, 420], [342, 420], [404, 420], [466, 420]]]

juego = {"tipo de mensaje": "juego", "tablero": matriz_batalla,
         "jugador1": {"nombre": "Nandel", "pokemones_tablero": [],
                      "pokemones_banca": [{"nombre": mis_pokemones[0]}, {"nombre": mis_pokemones[1]},
                                          {"nombre": mis_pokemones[2]}]},
         "jugador2": {"nombre": "Nandel", "pokemones_tablero": [],
                      "pokemones_banca": [{"nombre": sus_pokemones[0]}, {"nombre": sus_pokemones[1]},
                                          {"nombre": sus_pokemones[2]}]}}

lucha = {"tipo_mensaje": "lucha", "jugador1": {"nombre": "", "pokemon": {"nombre": "", "ataque": 0}},
         "jugador2": {"nombre": "", "pokemon": {"nombre": "", "ataque": 0}}}

def ubicar_pokemon(event):
    for x in range(len(juego["jugador1"]["pokemones_tablero"])):
        for y in mis_pokemones:
            if juego["jugador1"]["pokemones_tablero"][x]["nombre"] == y:
                fila = juego["jugador1"]["pokemones_tablero"][x]["posicion"][0]
                columna = juego["jugador1"]["pokemones_tablero"][x]["posicion"][1]
                ubicacion_matriz = matriz_coords[fila][columna]
                borrar_matriz_coords_batalla(y)
                canvas_fondo_batalla.coords(y, ubicacion_matriz[0], ubicacion_matriz[1])
                matriz_coords_batalla(y, "cursor")
            elif juego["jugador2"]["pokemones_tablero"][x]["nombre"] == y:
                fila = juego["jugador2"]["pokemones_tablero"][x]["posicion"][0]
                columna = juego["jugador2"]["pokemones_tablero"][x]["posicion"][1]
                ubicacion_matriz = matriz_coords[fila][columna]
                borrar_matriz_coords_batalla(y)
                canvas_fondo_batalla.coords(y, ubicacion_matriz[0], ubicacion_matriz[1])
                matriz_coords_batalla(y, "cursor2")
    return matriz_batalla


turnos_jugadores(1)
ventana_batalla.mainloop()
