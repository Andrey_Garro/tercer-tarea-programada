from tkinter import *
import os
from threading import Thread
import socket
import pygame
import random

server = "no"
cliente = "no"


def ciudad_plateada(ventana_principal, pokedex_pokemon, nombre_pj):
    """
    Esta funcion crea todo lo que se vera en ciudad plateada
    :param ventana_principal: es la ventana en la que se crea el canvas de esta ciudad
    :param pokedex_pokemon: el pokedex donde se ven los pokemones atrapados
    :param nombre_pj: el nombre del personaje
    :return: 
    """
    global numero_imagen_ash, numero_imagen_pika
    numero_imagen_pika = 0
    numero_imagen_ash = 0

    pygame.mixer.music.stop()
    pygame.mixer.music.load("mus_ciudad_plateada.ogg")
    pygame.mixer.music.play(10)



    canvas_ciudad_plateda = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
    canvas_ciudad_plateda.config(bg="pink")
    canvas_gimnasio = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
    canvas_gimnasio.config(bg="pink")
    canvas_pokemon_duel = Canvas(ventana_principal, width=718, height=357, bd=0, highlightthickness=0)
    canvas_pokemon_duel.config(bg="pink")
    canvas_pokemon_duel_servidor = Canvas(ventana_principal, width=718, height=357, bd=0, highlightthickness=0)
    canvas_pokemon_duel_servidor.config(bg="pink")
    canvas_pokemon_duel_cliente = Canvas(ventana_principal, width=718, height=357, bd=0, highlightthickness=0)
    canvas_pokemon_duel_servidor.config(bg="pink")

    def load_img(name):
        """
        carga las imagenes al programa
        :param name: nombre de la imagen
        :return: retorna la imagen deseada
        """
        path = os.path.join("imgs", name)
        img = PhotoImage(file=path)
        return img

    # listas de los sprites de movimiento del personaje y pikachu
    mover_arriba1, mover_derecha1, mover_izq1 = [load_img("ash_e_1.png"), load_img("ash_e_2.png"),
                                                 load_img("ash_e_3.png"), ], \
                                                [load_img("ash_d_2.png"), load_img("ash_d_2.png"),
                                                 load_img("ash_d_3.png"), ], \
                                                [load_img("ash_i_1.png"), load_img("ash_i_2.png"),
                                                 load_img("ash_i_2.png")]

    mover_abajo1 = [load_img("ash_f_1.png"), load_img("ash_f_2.png"), load_img("ash_f_3.png")]

    mover_arriba2, mover_abajo2, mover_derecha2, mover_izq2 = [load_img("pikachu_e_1.png"), load_img("pikachu_e_2.png"),
                                                               load_img("pikachu_e_3.png"), ], \
                                                              [load_img("pikachu_f_1.png"), load_img("pikachu_f_2.png"),
                                                               load_img("pikachu_f_3.png")], \
                                                              [load_img("pikachu_d_2.png"), load_img("pikachu_d_2.png"),
                                                               load_img("pikachu_d_3.png"), ], \
                                                              [load_img("pikachu_i_1.png"), load_img("pikachu_i_2.png"),
                                                               load_img("pikachu_i_2.png")]

    # calcular_coords_x
    # Entradas: coordenada x1 y x2 del objeto
    # Salidas: una lista con todas las posibles coordenadas de x en el objeto
    def calcular_coords_x(coordenada_x1, coordenada_x2, lista_coordx):
        if coordenada_x1 > coordenada_x2:
            return lista_coordx
        else:
            lista_coordx.append(coordenada_x1)
            return calcular_coords_x(coordenada_x1 + 1, coordenada_x2, lista_coordx)

    # calcular_coords_y
    # Entradas: coordenada y1 y y2 del objeto
    # Salidas: una lista con todas las posibles coordenadas de y en el objeto
    def calcular_coords_y(coordenada_y1, coordenada_y2, lista_coordy):
        if coordenada_y1 > coordenada_y2:
            return lista_coordy
        else:
            lista_coordy.append(coordenada_y1)
            return calcular_coords_y(coordenada_y1 + 1, coordenada_y2, lista_coordy)

    rec1 = canvas_ciudad_plateda.create_rectangle(0, 0, 33, 550)
    rec2 = canvas_ciudad_plateda.create_rectangle(0, 150, 75, 550)
    rec3 = canvas_ciudad_plateda.create_rectangle(0, 0, 850, 20)
    rec4 = canvas_ciudad_plateda.create_rectangle(200, 0, 850, 130)
    rec5 = canvas_ciudad_plateda.create_rectangle(790, 0, 850, 320)
    rec6 = canvas_ciudad_plateda.create_rectangle(790, 375, 850, 550)
    rec7 = canvas_ciudad_plateda.create_rectangle(770, 306, 790, 320)
    rec8 = canvas_ciudad_plateda.create_rectangle(495, 360, 705, 420)
    rec9 = canvas_ciudad_plateda.create_rectangle(495, 420, 507, 500)
    rec10 = canvas_ciudad_plateda.create_rectangle(715, 405, 727, 500)
    rec11 = canvas_ciudad_plateda.create_rectangle(715, 405, 705, 420)
    rec12 = canvas_ciudad_plateda.create_rectangle(640, 500, 715, 490)
    rec13 = canvas_ciudad_plateda.create_rectangle(500, 500, 600, 490)
    rec14 = canvas_ciudad_plateda.create_rectangle(250, 325, 410, 420)
    rec15 = canvas_ciudad_plateda.create_rectangle(100, 415, 250, 500)
    rec16 = canvas_ciudad_plateda.create_rectangle(0, 325, 410, 340)
    rec17 = canvas_ciudad_plateda.create_rectangle(400, 275, 410, 340)
    rec18 = canvas_ciudad_plateda.create_rectangle(300, 195, 410, 275)
    rec19 = canvas_ciudad_plateda.create_rectangle(250, 195, 300, 220)
    rec20 = canvas_ciudad_plateda.create_rectangle(140, 195, 250, 275)
    rec21 = canvas_ciudad_plateda.create_rectangle(140, 120, 200, 130)
    rec22 = canvas_ciudad_plateda.create_rectangle(307, 130, 325, 140)
    rec23 = canvas_ciudad_plateda.create_rectangle(780, 320, 850, 380)
    rec24 = canvas_ciudad_plateda.create_rectangle(0, 552, 850, 552)
    rec_gym = canvas_ciudad_plateda.create_rectangle(250, 220, 300, 275)


    rec1_bbox = canvas_ciudad_plateda.bbox(rec1)
    rec2_bbox = canvas_ciudad_plateda.bbox(rec2)
    rec3_bbox = canvas_ciudad_plateda.bbox(rec3)
    rec4_bbox = canvas_ciudad_plateda.bbox(rec4)
    rec5_bbox = canvas_ciudad_plateda.bbox(rec5)
    rec6_bbox = canvas_ciudad_plateda.bbox(rec6)
    rec7_bbox = canvas_ciudad_plateda.bbox(rec7)
    rec8_bbox = canvas_ciudad_plateda.bbox(rec8)
    rec9_bbox = canvas_ciudad_plateda.bbox(rec9)
    rec10_bbox = canvas_ciudad_plateda.bbox(rec10)
    rec11_bbox = canvas_ciudad_plateda.bbox(rec11)
    rec12_bbox = canvas_ciudad_plateda.bbox(rec12)
    rec13_bbox = canvas_ciudad_plateda.bbox(rec13)
    rec14_bbox = canvas_ciudad_plateda.bbox(rec14)
    rec15_bbox = canvas_ciudad_plateda.bbox(rec15)
    rec16_bbox = canvas_ciudad_plateda.bbox(rec16)
    rec17_bbox = canvas_ciudad_plateda.bbox(rec17)
    rec18_bbox = canvas_ciudad_plateda.bbox(rec18)
    rec19_bbox = canvas_ciudad_plateda.bbox(rec19)
    rec20_bbox = canvas_ciudad_plateda.bbox(rec20)
    rec21_bbox = canvas_ciudad_plateda.bbox(rec21)
    rec22_bbox = canvas_ciudad_plateda.bbox(rec22)
    rec23_bbox = canvas_ciudad_plateda.bbox(rec23)
    rec24_bbox = canvas_ciudad_plateda.bbox(rec24)
    rec_gym_bbox = canvas_ciudad_plateda.bbox(rec_gym)

    lista_objetos_plateada = [rec1_bbox, rec2_bbox, rec3_bbox, rec4_bbox, rec5_bbox, rec6_bbox, rec7_bbox, rec8_bbox,
                              rec9_bbox, rec10_bbox, rec11_bbox, rec12_bbox, rec13_bbox,
                              rec14_bbox, rec15_bbox, rec16_bbox, rec17_bbox, rec18_bbox, rec19_bbox, rec20_bbox,
                              rec21_bbox, rec22_bbox, rec23_bbox, rec24_bbox]

    ciudad_plateada_img = load_img("ciudad_plateada.png")
    pika1 = load_img("pikachu_e_1.png")
    ash1 = load_img("ash_e_1.png")
    plateada = canvas_ciudad_plateda.create_image(0, 0, anchor=NW, image=ciudad_plateada_img)
    pika_plateada = canvas_ciudad_plateda.create_image(445, 540, anchor=CENTER, image=pika1)
    ash_plateada = canvas_ciudad_plateda.create_image(410, 530, anchor=CENTER, image=ash1)
    techo = load_img("techo_gym.png")
    techo_gym = canvas_ciudad_plateda.create_image(274, 208, anchor=CENTER, image=techo)

    # movimiento_plateada
    # Entradas: ninguna
    # Salidas: funciones con los movimientos de los personajes en ciudad plateada
    def movimiento_plateada():

        # Asignacion de la variable para las transiciones de ash y pikachu
        # movimiento_ash_plateada
        # Entradas: sprites de los movimientos del personaje dependiendo hacia donde se mueva y el fondo del escenario
        # Salidas: el sprite del movimiento del personaje
        def movimiento_ash_plateada(movimientos, fondo):
            global numero_imagen_ash
            numero_imagen_ash += 1
            if numero_imagen_ash == len(movimientos):
                numero_imagen_ash = 0
            fondo.itemconfig(ash_plateada, image=movimientos[numero_imagen_ash])

        # movimiento_pika_plateada
        # Entradas: sprites de los movimientos de pikachu dependiendo hacia donde se mueva y el fondo del escenario
        # Salidas: el sprite del movimiento de pikachu
        def movimiento_pika_plateada(movimientos, fondo):
            global numero_imagen_pika
            numero_imagen_pika += 1
            if numero_imagen_pika == len(movimientos):
                numero_imagen_pika = 0
            fondo.itemconfig(pika_plateada, image=movimientos[numero_imagen_pika])

        # colisiones_arriba_plateada_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de pikachu
        def colisiones_arriba_plateada_pika(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][1] <= canvas_ciudad_plateda.coords(pika_plateada)[1] - 18 < lista_objetos[0][3]:
                if canvas_ciudad_plateda.coords(pika_plateada)[0] in calcular_coords_x(lista_objetos[0][0],
                                                                                       lista_objetos[0][2], []):
                    return False
                else:
                    return colisiones_arriba_plateada_pika(lista_objetos[1:])
            else:
                return colisiones_arriba_plateada_pika(lista_objetos[1:])

        # colisiones_abajo_plateada_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de pikachu
        def colisiones_abajo_plateada_pika(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][1] <= canvas_ciudad_plateda.coords(pika_plateada)[1] + 18 < lista_objetos[0][3]:
                if canvas_ciudad_plateda.coords(pika_plateada)[0] in calcular_coords_x(lista_objetos[0][0],
                                                                                       lista_objetos[0][2], []):
                    return False
                else:
                    return colisiones_abajo_plateada_pika(lista_objetos[1:])
            else:
                return colisiones_abajo_plateada_pika(lista_objetos[1:])

        # colisiones_derecha_plateada_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de pikachu
        def colisiones_derecha_plateada_pika(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][0] <= canvas_ciudad_plateda.coords(pika_plateada)[0] + 18 < lista_objetos[0][2]:
                if canvas_ciudad_plateda.coords(pika_plateada)[1] in calcular_coords_y(lista_objetos[0][1],
                                                                                       lista_objetos[0][3], []):
                    return False
                else:
                    return colisiones_derecha_plateada_pika(lista_objetos[1:])
            else:
                return colisiones_derecha_plateada_pika(lista_objetos[1:])

        # colisiones_arriba_plateada_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de pikachu
        def colisiones_izquierda_plateada_pika(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][0] <= canvas_ciudad_plateda.coords(pika_plateada)[0] - 18 < lista_objetos[0][2]:
                if canvas_ciudad_plateda.coords(pika_plateada)[1] in calcular_coords_y(lista_objetos[0][1],
                                                                                       lista_objetos[0][3], []):
                    return False
                else:
                    return colisiones_izquierda_plateada_pika(lista_objetos[1:])
            else:
                return colisiones_izquierda_plateada_pika(lista_objetos[1:])

        # colisiones_arriba_plateada_ash
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de ash
        def colisiones_arriba_plateada_ash(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][1] <= canvas_ciudad_plateda.coords(ash_plateada)[1] - 18 < lista_objetos[0][3]:
                if canvas_ciudad_plateda.coords(ash_plateada)[0] in calcular_coords_x(lista_objetos[0][0],
                                                                                      lista_objetos[0][2], []):
                    return False
                else:
                    return colisiones_arriba_plateada_ash(lista_objetos[1:])
            else:
                return colisiones_arriba_plateada_ash(lista_objetos[1:])

        # colisiones_abajo_plateada_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de ash
        def colisiones_abajo_plateada_ash(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][1] <= canvas_ciudad_plateda.coords(ash_plateada)[1] + 18 < lista_objetos[0][3]:
                if canvas_ciudad_plateda.coords(ash_plateada)[0] in calcular_coords_x(lista_objetos[0][0],
                                                                                      lista_objetos[0][2], []):
                    return False
                else:
                    return colisiones_abajo_plateada_ash(lista_objetos[1:])
            else:
                return colisiones_abajo_plateada_ash(lista_objetos[1:])

        # colisiones_derecha_plateada_ash
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de ash
        def colisiones_derecha_plateada_ash(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][0] <= canvas_ciudad_plateda.coords(ash_plateada)[0] + 18 < lista_objetos[0][2]:
                if canvas_ciudad_plateda.coords(ash_plateada)[1] in calcular_coords_y(lista_objetos[0][1],
                                                                                      lista_objetos[0][3], []):
                    return False
                else:
                    return colisiones_derecha_plateada_ash(lista_objetos[1:])
            else:
                return colisiones_derecha_plateada_ash(lista_objetos[1:])

        # colisiones_izquierda_plateada_ash
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de ash
        def colisiones_izquierda_plateada_ash(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][0] <= canvas_ciudad_plateda.coords(ash_plateada)[0] - 18 < lista_objetos[0][2]:
                if canvas_ciudad_plateda.coords(ash_plateada)[1] in calcular_coords_y(lista_objetos[0][1],
                                                                                      lista_objetos[0][3], []):
                    return False
                else:
                    return colisiones_izquierda_plateada_ash(lista_objetos[1:])
            else:
                return colisiones_izquierda_plateada_ash(lista_objetos[1:])

        # mover_arriba
        # Entradas: evento de la tecla arriba
        # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
        def mover_arriba(event):
            if rec_gym_bbox[0] >= canvas_ciudad_plateda.coords(ash_plateada)[0] - 18 <= rec_gym_bbox[2]:
                if canvas_ciudad_plateda.coords(ash_plateada)[0] in calcular_coords_x(rec_gym_bbox[0],
                                                                                      rec_gym_bbox[2], []):
                    return gimnasio()
            if colisiones_arriba_plateada_ash(lista_objetos_plateada) == False or colisiones_arriba_plateada_pika(
                    lista_objetos_plateada) == False:
                movimiento_ash_plateada(mover_arriba1, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(ash_plateada, 0, 0)
                movimiento_pika_plateada(mover_arriba2, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(pika_plateada, 0, 0)
            else:
                movimiento_ash_plateada(mover_arriba1, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(ash_plateada, 0, -7)
                movimiento_pika_plateada(mover_arriba2, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(pika_plateada, 0, -7)

        # mover_abajo
        # Entradas: evento de la tecla abajo
        # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
        def mover_abajo(event):
            if colisiones_abajo_plateada_ash(lista_objetos_plateada) == False or colisiones_abajo_plateada_pika(
                    lista_objetos_plateada) == False:
                movimiento_ash_plateada(mover_abajo1, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(ash_plateada, 0, 0)
                movimiento_pika_plateada(mover_abajo2, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(pika_plateada, 0, 0)
            else:
                movimiento_ash_plateada(mover_abajo1, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(ash_plateada, 0, 7)
                movimiento_pika_plateada(mover_abajo2, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(pika_plateada, 0, 7)

        # mover_derecha
        # Entradas: evento de la tecla derecha
        # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
        def mover_derecha(event):
            if colisiones_derecha_plateada_ash(lista_objetos_plateada) == False or colisiones_derecha_plateada_pika(
                    lista_objetos_plateada) == False:
                movimiento_ash_plateada(mover_derecha1, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(ash_plateada, 0, 0)
                movimiento_pika_plateada(mover_derecha2, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(pika_plateada, 0, 0)
            else:
                movimiento_ash_plateada(mover_derecha1, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(ash_plateada, 7, 0)
                movimiento_pika_plateada(mover_derecha2, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(pika_plateada, 7, 0)

        # mover_izquierda
        # Entradas: evento de la tecla izquierda
        # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
        def mover_izquierda(event):
            if colisiones_izquierda_plateada_ash(lista_objetos_plateada) == False or colisiones_izquierda_plateada_pika(
                    lista_objetos_plateada) == False:
                movimiento_ash_plateada(mover_izq1, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(ash_plateada, 0, 0)
                movimiento_pika_plateada(mover_izq2, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(pika_plateada, 0, 0)
            else:
                movimiento_ash_plateada(mover_izq1, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(ash_plateada, -7, 0)
                movimiento_pika_plateada(mover_izq2, canvas_ciudad_plateda)
                canvas_ciudad_plateda.move(pika_plateada, -7, 0)

        ventana_principal.bind("<Up>", mover_arriba)
        ventana_principal.bind("<Left>", mover_izquierda)
        ventana_principal.bind("<Right>", mover_derecha)
        ventana_principal.bind("<Down>", mover_abajo)

    gimnasio_img = load_img("gimnasio.png")
    pared = load_img("pared_gym.png")

    def crear_plateada_desde_gym():
        canvas_gimnasio.forget()
        pygame.mixer.music.stop()
        pygame.mixer.music.load("mus_ciudad_plateada.ogg")
        pygame.mixer.music.play(10)
        movimiento_plateada()
        canvas_ciudad_plateda.pack()

    def gimnasio():
        """
        funcion que llama a todo o que hay dentro del gimnasio
        :return: crea el canvas del gimnasio y todo lo que hay en el
        """
        pygame.mixer.music.stop()
        pygame.mixer.music.load("mus_gimnasio.ogg")
        pygame.mixer.music.play(10)
        global ash_gym, pika_gym, lista_objetos_gym
        canvas_ciudad_plateda.forget()
        rec1_gym = canvas_gimnasio.create_rectangle(0, 0, 243, 548)
        rec2_gym = canvas_gimnasio.create_rectangle(241, 0, 294, 135)
        rec3_gym = canvas_gimnasio.create_rectangle(280, 0, 377, 106)
        rec4_gym = canvas_gimnasio.create_rectangle(381, 0, 490, 63)
        rec5_gym = canvas_gimnasio.create_rectangle(490, 0, 590, 103)
        rec6_gym = canvas_gimnasio.create_rectangle(572, 103, 607, 135)
        rec7_gym = canvas_gimnasio.create_rectangle(607, 135, 850, 550)
        rec8_gym = canvas_gimnasio.create_rectangle(504, 167, 626, 409)
        rec9_gym = canvas_gimnasio.create_rectangle(435, 304, 504, 355)
        rec10_gym = canvas_gimnasio.create_rectangle(297, 332, 362, 409)
        rec11_gym = canvas_gimnasio.create_rectangle(347, 437, 381, 495)
        rec12_gym = canvas_gimnasio.create_rectangle(485, 436, 520, 494)
        rec13_gym = canvas_gimnasio.create_rectangle(244, 83, 260, 550)
        rec14_gym = canvas_gimnasio.create_rectangle(244, 83, 295, 136)
        rec15_gym = canvas_gimnasio.create_rectangle(330, 166, 502, 191)
        rec16_gym = canvas_gimnasio.create_rectangle(261, 548, 402, 550)
        rec17_gym = canvas_gimnasio.create_rectangle(465, 548, 627, 550)
        rec1_bbox_gym = canvas_gimnasio.bbox(rec1_gym)
        rec2_bbox_gym = canvas_gimnasio.bbox(rec2_gym)
        rec3_bbox_gym = canvas_gimnasio.bbox(rec3_gym)
        rec4_bbox_gym = canvas_gimnasio.bbox(rec4_gym)
        rec5_bbox_gym = canvas_gimnasio.bbox(rec5_gym)
        rec6_bbox_gym = canvas_gimnasio.bbox(rec6_gym)
        rec7_bbox_gym = canvas_gimnasio.bbox(rec7_gym)
        rec8_bbox_gym = canvas_gimnasio.bbox(rec8_gym)
        rec9_bbox_gym = canvas_gimnasio.bbox(rec9_gym)
        rec10_bbox_gym = canvas_gimnasio.bbox(rec10_gym)
        rec11_bbox_gym = canvas_gimnasio.bbox(rec11_gym)
        rec12_bbox_gym = canvas_gimnasio.bbox(rec12_gym)
        rec13_bbox_gym = canvas_gimnasio.bbox(rec13_gym)
        rec14_bbox_gym = canvas_gimnasio.bbox(rec14_gym)
        rec15_bbox_gym = canvas_gimnasio.bbox(rec15_gym)
        rec16_bbox_gym = canvas_gimnasio.bbox(rec16_gym)
        rec17_bbox_gym = canvas_gimnasio.bbox(rec17_gym)

        lista_objetos_gym = [rec1_bbox_gym, rec2_bbox_gym, rec3_bbox_gym, rec4_bbox_gym, rec5_bbox_gym, rec6_bbox_gym,
                             rec7_bbox_gym, rec8_bbox_gym, rec9_bbox_gym, rec10_bbox_gym, rec11_bbox_gym,
                             rec12_bbox_gym,
                             rec13_bbox_gym, rec14_bbox_gym, rec15_bbox_gym, rec16_bbox_gym, rec17_bbox_gym]
        gimnasio1 = canvas_gimnasio.create_image(0, 0, anchor=NW, image=gimnasio_img)
        pika_gym = canvas_gimnasio.create_image(445, 540, anchor=CENTER, image=pika1)
        ash_gym = canvas_gimnasio.create_image(410, 530, anchor=CENTER, image=ash1)
        pared_gym = canvas_gimnasio.create_image(470, 173, anchor=CENTER, image=pared)
        movimiento_gym()
        canvas_gimnasio.pack()

    # movimiento_gym
    # Entradas: ninguna
    # Salidas: funciones con los movimientos de los personajes en pueblo paleta
    def movimiento_gym():
        # movimiento_ash_gym
        # Entradas: sprites de los movimientos del personaje dependiendo hacia donde se mueva y el fondo del escenario
        # Salidas: el sprite del movimiento del personaje
        def movimiento_ash_gym(movimientos, fondo):
            global numero_imagen_ash
            numero_imagen_ash += 1
            if numero_imagen_ash == len(movimientos):
                numero_imagen_ash = 0
            fondo.itemconfig(ash_gym, image=movimientos[numero_imagen_ash])

        # movimiento_pika_gym
        # Entradas: sprites de los movimientos de pikachu dependiendo hacia donde se mueva y el fondo del escenario
        # Salidas: el sprite del movimiento de pikachu
        def movimiento_pika_gym(movimientos, fondo):
            global numero_imagen_pika
            numero_imagen_pika += 1
            if numero_imagen_pika == len(movimientos):
                numero_imagen_pika = 0
            fondo.itemconfig(pika_gym, image=movimientos[numero_imagen_pika])

        # colisiones_arriba_gym_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de pikachu
        def colisiones_arriba_gym_pika(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][1] <= canvas_gimnasio.coords(pika_gym)[1] - 18 < lista_objetos[0][3]:
                if canvas_gimnasio.coords(pika_gym)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2],
                                                                            []):
                    return False
                else:
                    return colisiones_arriba_gym_pika(lista_objetos[1:])
            else:
                return colisiones_arriba_gym_pika(lista_objetos[1:])

        # colisiones_abajo_paleta_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de pikachu
        def colisiones_abajo_gym_pika(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][1] <= canvas_gimnasio.coords(pika_gym)[1] + 18 < lista_objetos[0][3]:
                if canvas_gimnasio.coords(pika_gym)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2],
                                                                            []):
                    return False
                else:
                    return colisiones_abajo_gym_pika(lista_objetos[1:])
            else:
                return colisiones_abajo_gym_pika(lista_objetos[1:])

        # colisiones_derecha_gym_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de pikachu
        def colisiones_derecha_gym_pika(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][0] <= canvas_gimnasio.coords(pika_gym)[0] + 18 < lista_objetos[0][2]:
                if canvas_gimnasio.coords(pika_gym)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3],
                                                                            []):
                    return False
                else:
                    return colisiones_derecha_gym_pika(lista_objetos[1:])
            else:
                return colisiones_derecha_gym_pika(lista_objetos[1:])

        # colisiones_arriba_gym_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de pikachu
        def colisiones_izquierda_gym_pika(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][0] <= canvas_gimnasio.coords(pika_gym)[0] - 18 < lista_objetos[0][2]:
                if canvas_gimnasio.coords(pika_gym)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3],
                                                                            []):
                    return False
                else:
                    return colisiones_izquierda_gym_pika(lista_objetos[1:])
            else:
                return colisiones_izquierda_gym_pika(lista_objetos[1:])

        # colisiones_arriba_gym_ash
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de ash
        def colisiones_arriba_gym_ash(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][1] <= canvas_gimnasio.coords(ash_gym)[1] - 18 < lista_objetos[0][3]:
                if canvas_gimnasio.coords(ash_gym)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2],
                                                                           []):
                    return False
                else:
                    return colisiones_arriba_gym_ash(lista_objetos[1:])
            else:
                return colisiones_arriba_gym_ash(lista_objetos[1:])

        # colisiones_abajo_gym_pika
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de ash
        def colisiones_abajo_gym_ash(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][1] <= canvas_gimnasio.coords(ash_gym)[1] + 18 < lista_objetos[0][3]:
                if canvas_gimnasio.coords(ash_gym)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2],
                                                                           []):
                    return False
                else:
                    return colisiones_abajo_gym_ash(lista_objetos[1:])
            else:
                return colisiones_abajo_gym_ash(lista_objetos[1:])

        # colisiones_derecha_gym_ash
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de ash
        def colisiones_derecha_gym_ash(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][0] <= canvas_gimnasio.coords(ash_gym)[0] + 18 < lista_objetos[0][2]:
                if canvas_gimnasio.coords(ash_gym)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3],
                                                                           []):
                    return False
                else:
                    return colisiones_derecha_gym_ash(lista_objetos[1:])
            else:
                return colisiones_derecha_gym_ash(lista_objetos[1:])

        # colisiones_izquierda_gym_ash
        # Entradas: la lista con los bbox de los objetos en el mapa
        # Salidas: True o False dependiendo de la posicion de ash
        def colisiones_izquierda_gym_ash(lista_objetos):
            if len(lista_objetos) == 0:
                return True
            if lista_objetos[0][0] <= canvas_gimnasio.coords(ash_gym)[0] - 18 < lista_objetos[0][2]:
                if canvas_gimnasio.coords(ash_gym)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3],
                                                                           []):
                    return False
                else:
                    return colisiones_izquierda_gym_ash(lista_objetos[1:])
            else:
                return colisiones_izquierda_gym_ash(lista_objetos[1:])

        # mover_arriba
        # Entradas: evento de la tecla arriba
        # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
        def mover_arriba(event):
            if colisiones_arriba_gym_ash(lista_objetos_gym) == False or colisiones_arriba_gym_pika(
                    lista_objetos_gym) == False:
                movimiento_ash_gym(mover_arriba1, canvas_gimnasio)
                canvas_gimnasio.move(ash_gym, 0, 0)
                movimiento_pika_gym(mover_arriba2, canvas_gimnasio)
                canvas_gimnasio.move(pika_gym, 0, 0)
            else:
                movimiento_ash_gym(mover_arriba1, canvas_gimnasio)
                canvas_gimnasio.move(ash_gym, 0, -7)
                movimiento_pika_gym(mover_arriba2, canvas_gimnasio)
                canvas_gimnasio.move(pika_gym, 0, -7)

        # mover_abajo
        # Entradas: evento de la tecla abajo
        # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
        def mover_abajo(event):
            if canvas_gimnasio.coords(ash_gym)[1] >= 550:
                return crear_plateada_desde_gym()
            if colisiones_abajo_gym_ash(lista_objetos_gym) == False or colisiones_abajo_gym_pika(
                    lista_objetos_gym) == False:
                movimiento_ash_gym(mover_abajo1, canvas_gimnasio)
                canvas_gimnasio.move(ash_gym, 0, 0)
                movimiento_pika_gym(mover_abajo2, canvas_gimnasio)
                canvas_gimnasio.move(pika_gym, 0, 0)
            else:
                movimiento_ash_gym(mover_abajo1, canvas_gimnasio)
                canvas_gimnasio.move(ash_gym, 0, 7)
                movimiento_pika_gym(mover_abajo2, canvas_gimnasio)
                canvas_gimnasio.move(pika_gym, 0, 7)

        # mover_derecha
        # Entradas: evento de la tecla derecha
        # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
        def mover_derecha(event):
            if colisiones_derecha_gym_ash(lista_objetos_gym) == False or colisiones_derecha_gym_pika(
                    lista_objetos_gym) == False:
                movimiento_ash_gym(mover_derecha1, canvas_gimnasio)
                canvas_gimnasio.move(ash_gym, 0, 0)
                movimiento_pika_gym(mover_derecha2, canvas_gimnasio)
                canvas_gimnasio.move(pika_gym, 0, 0)
            else:
                movimiento_ash_gym(mover_derecha1, canvas_gimnasio)
                canvas_gimnasio.move(ash_gym, 7, 0)
                movimiento_pika_gym(mover_derecha2, canvas_gimnasio)
                canvas_gimnasio.move(pika_gym, 7, 0)

        # mover_izquierda
        # Entradas: evento de la tecla izquierda
        # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
        def mover_izquierda(event):
            if colisiones_izquierda_gym_ash(lista_objetos_gym) == False or colisiones_izquierda_gym_pika(
                    lista_objetos_gym) == False:
                movimiento_ash_gym(mover_izq1, canvas_gimnasio)
                canvas_gimnasio.move(ash_gym, 0, 0)
                movimiento_pika_gym(mover_izq2, canvas_gimnasio)
                canvas_gimnasio.move(pika_gym, 0, 0)
            else:
                movimiento_ash_gym(mover_izq1, canvas_gimnasio)
                canvas_gimnasio.move(ash_gym, -7, 0)
                movimiento_pika_gym(mover_izq2, canvas_gimnasio)
                canvas_gimnasio.move(pika_gym, -7, 0)

        def entrar_batalla(event):
            if canvas_gimnasio.coords(ash_gym)[1] <= 75 and "<Return>":
                ventana_principal.unbind("<Return>")
                ventana_principal.unbind("<Up>")
                ventana_principal.unbind("<Left>")
                ventana_principal.unbind("<Right>")
                ventana_principal.unbind("<Down>")
                crear_canvas_pokemon_duel()

        ventana_principal.bind("<Return>", entrar_batalla)
        ventana_principal.bind("<Up>", mover_arriba)
        ventana_principal.bind("<Left>", mover_izquierda)
        ventana_principal.bind("<Right>", mover_derecha)
        ventana_principal.bind("<Down>", mover_abajo)

    img_fondo = load_img("imagen_principal_pc.png")
    pokeball_sp = load_img("pokeball_sprite.png")
    img_servidor = load_img("servidor_pc.png")
    img_cliente = load_img("cliente_pc.png")

    def salir_pc():
        """
        esta funcion sale de la pc y vuelve al gimnasio
        :return: elimina el canvas de la pc y vuelve al gimnasio
        """
        canvas_pokemon_duel.place(x=9999999999, y=9999999999)
        movimiento_gym()

    def cerrar_cliente():
        """
        esta funcion cierra el canvas cliente y abre el canvas del computador
        :return: cierra el canvas cliente y abre el canvas del computador
        """
        canvas_pokemon_duel_cliente.place(x=9999999999, y=9999999999)
        canvas_pokemon_duel.place(x=70, y=100)

    def cerrar_servidor():
        """
        Esta funcion cierra el canvas del servidor y abre el del computador
        :return: cierra el canvas del servidor y abre el del computador
        """
        canvas_pokemon_duel_servidor.place(x=9999999999, y=9999999999)
        canvas_pokemon_duel.place(x=70, y=100)


    def crear_img_servidor():
        """
        Esta funcion crea y muestra el canvas del servidor
        :return: canvas servidor
        """

        def esperar_conexion():
            """
            habilita el puerto y acepta una conexion
            :return: 
            """
            s.bind(("", 6969))
            s.listen(1)
            print("Esperando conexión...")
            sc, addr = s.accept()
            print("Cliente conectado desde: ", addr)
            ir_a_batalla("listo")


        canvas_pokemon_duel.forget()
        host_name = socket.gethostname()
        host_ip = socket.gethostbyname(host_name)
        lbl_dir_ip = Label(canvas_pokemon_duel_servidor, text=host_ip, font=("Papyrus", 18), fg="red",
                           relief=RAISED).place(x=320, y=250)
        lbl_dir_ip1 = Label(canvas_pokemon_duel_servidor, text="Direccion IP:", font=("Papyrus", 18), fg="red",
                            relief=RAISED).place(x=150, y=250)
        lbl_puerto = Label(canvas_pokemon_duel_servidor, text="Puerto:", font=("Papyrus", 18), fg="red",
                           relief=RAISED).place(x=200, y=300)
        lbl_puerto1 = Label(canvas_pokemon_duel_servidor, text="6969", font=("Papyrus", 18), fg="red",
                            relief=RAISED).place(x=370, y=300)
        img_servidor1 = canvas_pokemon_duel_servidor.create_image(0, 0, anchor=NW, image=img_cliente)
        boton_atras = Button(canvas_pokemon_duel_servidor, width=10, height=2, command=cerrar_servidor,
                             text="Atras").place(x=630, y=50)
        boton_esperar = Button(canvas_pokemon_duel_servidor, width=13, height=2, command=esperar_conexion,
                               text="Iniciar Conexion").place(x=630, y=300)
        canvas_pokemon_duel_servidor.place(x=70, y=100)

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


    def crear_img_cliente():
        """
        Esta funcion crea y muestra el canvas del cliente
        :return: canvas cliente
        """

        def conectar_con_server():
            """
            conecta con el server
            :return: inicia la conexion al server
            """
            entry_ip = ip.get()
            entry_puerto = puerto.get()
            if entry_ip != "" or entry_puerto != "":
                if 13 <= len(entry_ip) <= 15 and len(entry_puerto) == 4:
                    cliente = "listo"
                    entry_puerto = int(entry_puerto)
                    socket_cliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    socket_cliente.connect((entry_ip, entry_puerto))
                else:
                    return
            else:
                return

        global ip, puerto, lbl_dir_ip1
        canvas_pokemon_duel.forget()
        img_cliente = canvas_pokemon_duel_cliente.create_image(0, 0, anchor=NW, image=img_servidor)
        entrada_puerto = StringVar()
        entrada_ip = StringVar()
        lbl_dir_ip1 = Label(canvas_pokemon_duel_cliente, text="Direccion IP:", font=("Papyrus", 18), fg="red",
                            relief=RAISED).place(
            x=50, y=250)
        lbl_puerto = Label(canvas_pokemon_duel_cliente, text="Puerto:", font=("Papyrus", 18), fg="red",
                           relief=RAISED).place(x=200,
                                                y=250)
        puerto = Entry(canvas_pokemon_duel_cliente, textvariable=entrada_puerto)
        puerto.place(x=200, y=300)
        ip = Entry(canvas_pokemon_duel_cliente, textvariable=entrada_ip)
        ip.place(x=50, y=300)
        boton_atras = Button(canvas_pokemon_duel_cliente, width=10, height=2, command=cerrar_cliente,
                             text="Atras").place(x=630, y=50)
        conectar = Button(canvas_pokemon_duel_cliente, width=10, height=2, command=conectar_con_server,
                          text="Conectar").place(x=630, y=200)
        canvas_pokemon_duel_cliente.place(x=70, y=100)

    def crear_canvas_pokemon_duel():
        """
        funcion que crea la "pantalla de la pc" para seleccionar entre cliente o servidor
        :return: canvas cliente o servidor
        """
        global boton_servidor, boton_salir, boton_cliente
        img_principal = canvas_pokemon_duel.create_image(0, 0, anchor=NW, image=img_fondo)
        boton_servidor = Button(canvas_pokemon_duel, width=10, height=2, command=thread_conexion, text="Servidor",
                                activebackground="Blue").place(
            x=530, y=300)
        boton_cliente = Button(canvas_pokemon_duel, width=10, height=2, command=crear_img_cliente,
                               text="Cliente").place(x=630, y=300)
        boton_salir = Button(canvas_pokemon_duel, width=10, height=2, command=salir_pc,
                             text="Salir").place(x=630, y=50)
        canvas_pokemon_duel.place(x=70, y=100)


    def thread_conexion():
        conectar = Thread(target= crear_img_servidor())
        fondo = Thread(target= crear_canvas_pokemon_duel())
        conectar.start()
        fondo.start()


    movimiento_plateada()
    canvas_ciudad_plateda.pack()
    ventana_principal.mainloop()

