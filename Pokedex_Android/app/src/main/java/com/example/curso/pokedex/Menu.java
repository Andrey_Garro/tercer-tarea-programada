package com.example.curso.pokedex;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        Button pokedexBtn = (Button) findViewById(R.id.btn_pokedex);
        pokedexBtn.setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            public void onClick(View arg0) {
                startActivity(new Intent(Menu.this, MainActivity.class));
            }
        });

        Button controlBtn = (Button) findViewById(R.id.btn_control);
        controlBtn.setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            public void onClick(View arg0) {
                startActivity(new Intent(Menu.this, ControlActivity.class));
            }
        });
    }
}