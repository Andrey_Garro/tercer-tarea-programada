package com.example.curso.pokedex;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.goebl.david.Webb;

import java.io.ByteArrayOutputStream;

public class ControlActivity extends AppCompatActivity {
    public String movimiento;
    private UploadImageTask mUploadImageTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        Button arribaBtn = (Button) findViewById(R.id.btn_arriba);
        Button abajoBtn = (Button) findViewById(R.id.btn_abajo);
        Button izquierdaBtn = (Button) findViewById(R.id.btn_izquierda);
        Button derechaBtn = (Button) findViewById(R.id.btn_derecha);
        Button startBtn = (Button) findViewById(R.id.btn_Start);
        Button A_Btn = (Button) findViewById(R.id.btn_A);
        Button B_Btn = (Button) findViewById(R.id.btn_B);
        arribaBtn.setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            public void onClick(View arg0) {
                String movimiento = "arriba";
                new Bundle().putString("movimiento", movimiento);
                mUploadImageTask = new UploadImageTask(movimiento);
                mUploadImageTask.execute((Void) null);
            }

         });
        abajoBtn.setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            public void onClick(View arg0) {
                String movimiento = "abajo";
                new Bundle().putString("movimiento", movimiento);
                mUploadImageTask = new UploadImageTask(movimiento);
                mUploadImageTask.execute((Void) null);
            }

        });
        derechaBtn.setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            public void onClick(View arg0) {
                String movimiento = "derecha";
                new Bundle().putString("movimiento", movimiento);
                mUploadImageTask = new UploadImageTask(movimiento);
                mUploadImageTask.execute((Void) null);
            }

        });
        izquierdaBtn.setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            public void onClick(View arg0) {
                String movimiento = "izquierda";
                new Bundle().putString("movimiento", movimiento);
                mUploadImageTask = new UploadImageTask(movimiento);
                mUploadImageTask.execute((Void) null);
            }

        });
        startBtn.setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            public void onClick(View arg0) {
                String movimiento = "start";
                new Bundle().putString("movimiento", movimiento);
                mUploadImageTask = new UploadImageTask(movimiento);
                mUploadImageTask.execute((Void) null);
            }

        });
        A_Btn.setOnClickListener(new Button.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            public void onClick(View arg0) {
                String movimiento = "espacio";
                new Bundle().putString("movimiento", movimiento);
                mUploadImageTask = new UploadImageTask(movimiento);
                mUploadImageTask.execute((Void) null);
            }

        });
    }
    private class UploadImageTask extends AsyncTask<Void, Void, Boolean> {
        private String mPokemonName = new Bundle().getString("movimiento");

        UploadImageTask(String movimiento) {
            this.mPokemonName = movimiento;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Webb webb = Webb.create();
                String response = webb.post("http://192.168.0.10:8080/api/v1/control")
                        .param("movimiento", mPokemonName)
                        .asString()
                        .getBody();
                return response.equals("true");
            } catch (Exception ex) {
                return false;
            }
        }
    }
}
